new Swiper('.swiper', {
	scrollbar: {
		el: '.swiper-scrollbar',
		draggable: true
	},

	mousewheel: {
		sensitivity: 1,
		evenstTarget: ".swiper"
	},

	slidesPerView: 'auto',

	watchOverflow: true,

	spaceBetween: 23.3333,

	freeMode: true
});

