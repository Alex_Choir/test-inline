function imageTextHide() {
	const imageText = document.querySelector(".image-block__description");

	if (window.innerWidth <= 768) {

		imageText.classList.remove('_hide');
	}
	else {
		imageText.classList.add('_hide');
	}
}

imageTextHide();
window.addEventListener('resize', function (event) {
	imageTextHide();
})

