function boxItemValueHide() {
	const valueText = document.querySelectorAll(".box__item--value");

	if (window.innerWidth <= 768) {
		valueText[1].textContent = 'Наталья и Юрий Лейшан';
	}
	else {
		valueText[1].textContent = 'Фермеры: Наталья и Юрий Лейшан';
	}
}

boxItemValueHide();
window.addEventListener('resize', function (event) {
	boxItemValueHide();
})