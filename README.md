# test-inline

Репозиторий для тестового задания для ООО Инлайн.

## развертывание проекта

Действия необходимо выполнять по порядку:

* Склонировать репозиторий

```sh
git clone https://gitlab.com/Alex_Choir/test-inline.git
```

* Перейти в директорию проекта, подтянуть npm зависимости (необходим установленный [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) на машине)

```
cd test-inline
npm install
```

* Собрать проект

```
npm run build
```

* Открыть `dist/index.html` в любом удобном браузере

